<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Utility;

class Utility{
    
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/others/index.php"){
        header("Location:".$url);
    }
    
    static public function Message($message = null){
        if(is_null($message)){
            $message = self::getMessage();
            return $message;
        }  else {
            self::setMessage($message);
        }
    }
    static private function getMessage(){
        $_msg = $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_msg;
    }

    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
    
}