<?php

include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'others' . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "startup.php");

use App\Other\Other;
use App\Utility\Utility;

$book = new Other();
$books = $book->index();
$trs = "";

?>

                <?php
                $slno =0;
                foreach($books as $book):
                    $slno++;
                    $trs .="<tr>";
                    $trs .="<td>".$slno."</td>";
                    $trs .="<td>".$book->languages."</td>";
                    $trs .="<td>".$book->reference."</td>";
                 
                    $trs .="</tr>";
                 endforeach;   
                ?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List of Other's Info</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        table {
   width:60%;         
   
   }
        </style>
    
    </head>
    
    <body>
        <h1>List of Other's Info</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    
                    <th>Languages &dArr;</th>
                    
                    <th>Reference &dArr;</th>
                    
                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>
       
        
            

    </body>
</html>
BITM;
?>
<?php
require_once $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."others".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

