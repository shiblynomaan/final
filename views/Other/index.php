<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Other's</title>

        <!-- Bootstrap -->
        <link href="./../../assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'others' . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Other\Other;
        use App\Utility\Utility;

$book = new Other();
        $books = $book->index();
        ?>

        <div class="container">

            <h3 class="text-center text-success">Other's Info</h3>
            <hr>
            <div id="first-container">


                <div id="msg" style="background-color: #46b8da; color: #F00; font-size: 25px;">

                    <?php echo Utility::message(); ?>     
                </div>
                <div class="row">
                      <div class="col-md-2">

                    <select class="simple-pagination-items-per-page"></select> Items Per Page.


                </div>
      
                <div class="col-md-5">
                    <a href="language.php" class="btn btn-success ">Add Info</a>
                    <a href="pdf.php" class="btn btn-success ">Pdf Download</a>
                    <a href="xl.php" class="btn btn-success ">XL Download</a>
                </div>
                <div class="col-md-5">
                    <form  class="form-inline"action="search.php?go" method="post" id="searchform">

                        <div class="input-group">
                            <input type="text" id="search" name="name" class="form-control" placeholder="Search with reference names...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" name="submit" value="search" type="submit">Go!</button>
                            </span>
                        </div> 

                    </form>
                </div>
                    
                </div>
              
        

                <hr>
                <table class="table table-bordered table-hover text-center bg-info">
                    <thead >
                        <tr>

                            <th class="text-center">SL No</th>
                            <th class="text-center">Languages</th>
                            <th class="text-center">Reference</th>
                            <th colspan="4" class="text-center">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sl = 1;
                        foreach ($books as $bk) {
                            ?>
                            <tr>

                                <td><?php echo $sl ?></td>
                                <td><?php echo $bk->languages; ?></td>
                                <td><?php echo $bk->reference; ?></td>
                                <td><a href="show.php?id=<?php echo $bk->id ?>" class="btn btn-success ">View</a></td>
                                <td><a href="edit.php?id=<?php echo $bk->id ?>" class="btn btn-primary ">Edit</a></td>
                                <td>
                                    <form action="delete.php" method="POST">
                                        <input type="hidden" name="id" value="<?php echo $bk->id; ?>"/>
                                        <button class="btn btn-danger delete" type="submit" >Delete</button>
                                    </form>
                                </td>
                                <td><a class="btn btn-default" href="mailer.php?id=<?php echo $bk->id ?>">Email Friend</a></td>
                            </tr>
                            <?php $sl++;
                            }
                            ?>
                    </tbody>
                </table>

                <a href="" class="pull-left "><button class="btn btn-danger" onclick="window.history.go(-1)">Back</button></a>


                <nav class="text-center">

                    <ul class="pagination my-navigation">

                        <li class="simple-pagination-first"><a href="#"></a></li>
                        <li class="simple-pagination-previous"><a href="#"></a></li>
                        <li class="simple-pagination-page-numbers"><a href="#"></a></li>
                        <li class="simple-pagination-next"><a href="#"></a></li>
                        <li class="simple-pagination-last"><a href="#"></a></li>

                    </ul>

                    <div class="simple-pagination-page-x-of-x text-center"></div>
                    <div class="simple-pagination-showing-x-of-x text-center"></div>
                </nav>
            </div>


        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        <script src="./../../assets/js/jquery-simple-pagination-plugin.js"></script>
        <script>
                $('.delete').bind('click', function (e) {
                    var dlt = confirm("Are you sure to Delete book Title");
                    if (!dlt) {
                        e.preventDefault();
                    }
                });
                $('#msg').fadeOut(5000);
                $('#first-container').simplePagination();
        </script>
    </body>
</html>