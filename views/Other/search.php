<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'others' . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "startup.php");
use App\Other\Other;
use App\Utility\Utility;

$phone = new Other();
$pp = $phone->search();
?>
<!DOCTYPE html>
<html lang = "en">

    <head>

        <meta charset = "utf-8">
        <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
        <meta name = "viewport" content = "width=device-width, initial-scale=1">
        <title>Phonebook</title>

        <!--CSS -->
        <link rel = "stylesheet" href = "http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel = "stylesheet" href = "./../../assets/css/bootstrap.min.css">
        <style>
            a {
                font-size: 16px;
                color: #000;
            }
            a:hover{

                text-decoration: none;

            }

        </style>


        <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">

                            <h1><strong>Other's Info</strong> Search Result</h1>
                           
                        </div>
                    </div>
                    <table class="table table-bordered table-hover text-center bg-info">
                        <thead>
                            <tr>
                                <th class="text-center">Sl.</th>
                                <th class="text-center">Languages</th>
                                <th class="text-center">Reference</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            if(count($pp)>0){
                            $slno = 1;
                            foreach ($pp as $p) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $p['languages']; ?></td>                             
                                    <td><?php echo $p['reference']; ?></td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            }else {
                            ?>
                                <tr>
                                    <td colspan="3">
                                        <h4><?php echo Utility::message(); ?> </h4> 
                                    </td>
                                </tr>
                                
                                
                                <?php
                            }
                                ?>
                        </tbody>
                    </table>
                    <nav style="padding: 0px 10px 0px 10px" class="text-center list-unstyled list-inline">
                        <li class="pull-left"><button class="btn btn-sm"> <a href="javascript:history.go(-1)">Back</a></button></li>
                        <li class="pull-right"><button class="btn btn-sm"> <a href="list.php">Go to List</a></button></li>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Javascript -->
        <script src="./../../../asset/js/jquery-1.11.1.min.js"></script>
        <script src="./../../../asset/js/bootstrap.min.js"></script>


        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>
